using FluentAssertions;
using MyVod.Domain.Movies;
using MyVod.Domain.Shared;

namespace MyVod.Domain.Tests.Movies
{
    internal class MovieAssert
    {
        private readonly Movie _movie;

        public MovieAssert(Movie movie)
        {
            _movie = movie;
        }

        public MovieAssert HaveTitleAndDescription(string title, string description)
        {
            _movie.Title.Should().Be(title);
            _movie.Description.Should().Be(description);
            return this;
        }
        
        public MovieAssert HavePrice(Money money)
        {
            _movie.Price.Should().Be(money);
            return this;
        }
        
        public MovieAssert BePublished()
        {
            _movie.Status.Should().Equals(Movie.MovieStatus.Published);
            return this;
        }
    }
}