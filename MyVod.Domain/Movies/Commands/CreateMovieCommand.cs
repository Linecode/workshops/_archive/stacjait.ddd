using Linecode.DDD;
using MediatR;

namespace MyVod.Domain.Movies.Commands
{
    public record CreateMovieCommand(string Title, string Description) : IRequest<Result<MovieId>>;
}