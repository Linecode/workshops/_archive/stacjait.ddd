using Linecode.DDD;
using MediatR;
using MyVod.Domain.Shared;

namespace MyVod.Domain.Movies.Commands
{
    public record SetMoviePriceCommand(MovieId MovieId, Money Money) : IRequest<Result>;
}