using Linecode.DDD;
using MediatR;

namespace MyVod.Domain.Movies.Commands
{
    public record GetMovieCommand(MovieId MovieId) : IRequest<Result<Movie>>;
}