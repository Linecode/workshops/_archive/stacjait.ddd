using System.Threading.Tasks;
using Linecode.DDD;

namespace MyVod.Domain.Movies.Repositories
{
    public interface IMoviesRepository
    {
        IUnitOfWork UnitOfWork { get; }
        void Add(Movie movie);
        Task<Movie> Get(MovieId movieId);
    }
}