using System;
using System.Threading;
using System.Threading.Tasks;
using Linecode.DDD;
using Linecode.DDD.Exceptions;
using MediatR;
using MyVod.Domain.Movies.Commands;
using MyVod.Domain.Movies.Policies;
using MyVod.Domain.Movies.Repositories;

namespace MyVod.Domain.Movies.Handlers
{
    public class PublishMovieHandler : IRequestHandler<PublishMovieCommand, Result>
    {
        private readonly IMoviesRepository _repository;

        public PublishMovieHandler(IMoviesRepository repository)
        {
            _repository = repository;
        }
        
        public async Task<Result> Handle(PublishMovieCommand request, CancellationToken cancellationToken)
        {
            var movie = await _repository.Get(request.MovieId);

            if (movie is null)
                throw new NotFoundException();

            var @event = new Movie.Events.PublishEvent
            {
                Policy = PublishPolicies.Default
            };

            try
            {
                movie.Apply(@event);
                await _repository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
            }
            catch (Exception e)
            {
                Result.Error(e);
            }
            
            return Result.Success();
        }
    }
}