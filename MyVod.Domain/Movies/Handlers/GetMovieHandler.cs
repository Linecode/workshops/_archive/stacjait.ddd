using System.Threading;
using System.Threading.Tasks;
using Linecode.DDD;
using Linecode.DDD.Exceptions;
using MediatR;
using MyVod.Domain.Movies.Commands;
using MyVod.Domain.Movies.Repositories;

namespace MyVod.Domain.Movies.Handlers
{
    public class GetMovieHandler : IRequestHandler<GetMovieCommand, Result<Movie>>
    {
        private readonly IMoviesRepository _repository;

        public GetMovieHandler(IMoviesRepository repository)
        {
            _repository = repository;
        }
        
        public async Task<Result<Movie>> Handle(GetMovieCommand request, CancellationToken cancellationToken)
        {
            var movie = await _repository.Get(request.MovieId);
            return Result<Movie>.Success(movie);
        }
    }
}