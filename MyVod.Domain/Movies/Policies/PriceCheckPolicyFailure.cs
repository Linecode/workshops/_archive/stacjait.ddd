using System;

namespace MyVod.Domain.Movies.Policies
{
    public class PriceCheckPolicyFailure : Exception {}
}