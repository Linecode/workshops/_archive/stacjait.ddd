using Linecode.DDD;

namespace MyVod.Domain.Movies.Policies
{
    public interface PublishPolicy
    {
        Result Publish(Movie movie);
    }
}