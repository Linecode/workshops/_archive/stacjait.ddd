using System.Threading.Tasks;
using Linecode.DDD;
using MyVod.Domain.Movies;
using MyVod.Domain.Movies.Repositories;

namespace MyVod.Infrastructure.Movies
{
    public class MoviesRepository : IMoviesRepository
    {
        private readonly MoviesContext _context;
        
        public IUnitOfWork UnitOfWork => _context;

        public MoviesRepository(MoviesContext context)
        {
            _context = context;
        }
        
        public void Add(Movie movie)
        {
            _context.Add(movie);
        }

        public async Task<Movie> Get(MovieId movieId)
        {
            return await _context.Movies.FindAsync(movieId);
        }
    }
}