using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Linecode.DDD;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyVod.Domain.Movies;
using Newtonsoft.Json;

namespace MyVod.Infrastructure.Movies
{
    public class MoviesContext : DbContext, IUnitOfWork
    {
        private readonly IMediator _mediator;

        public MoviesContext(DbContextOptions<MoviesContext> options, IMediator mediator) : base(options)
        {
            _mediator = mediator;
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        { 
            optionsBuilder.UseSqlServer(@"Server=localhost,1433;Database=MyVod;User Id=SA;Password=yourStrong(!)Password");
            optionsBuilder.UseLoggerFactory(MyLoggerFactory);
        }
        
        public DbSet<Movie> Movies { get; set; }
        public DbSet<OutboxMessage> Outbox { get; set; }
        
        public virtual async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            await _mediator.DispatchDomainEventsAsync<MoviesContext>(this).ConfigureAwait(false);
            
            await base.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
            return true;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(MoviesContext).Assembly);
            
            base.OnModelCreating(modelBuilder);
        }

        public static string Schema = "movies";
        
        static readonly ILoggerFactory MyLoggerFactory = LoggerFactory.Create(builder => { builder.AddConsole(); });
    }
}