using System;
using System.Threading.Tasks;
using Linecode.DDD.Exceptions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyVod.Api.Movies.Resources;
using MyVod.Domain.Movies;
using MyVod.Domain.Movies.Commands;
using MyVod.Domain.Movies.Policies;
using MyVod.Domain.Shared;

namespace MyVod.Api.Movies
{
    [ApiController]
    [Route("api/[controller]")]
    public class MoviesController : Controller
    {
        private readonly ISender _sender;

        public MoviesController(ISender sender)
        {
            _sender = sender;
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateMovieResource resource)
        {
            var result = await _sender.Send(new CreateMovieCommand(resource.Title, resource.Description));

            return result.Match<IActionResult>(
                success: data => Created(new Uri($"/api/movies/{data.Value}", UriKind.Relative), null),
                error: exception => StatusCode(500, exception));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(MovieId id)
        {
            var movie = await _sender.Send(new GetMovieCommand(id));

            return movie.Match<IActionResult>(
                success: data => Ok(MovieResource.From(data)), 
                error: exception => exception switch
                {
                    NotFoundException => NotFound(),
                    _ => StatusCode(StatusCodes.Status500InternalServerError, exception)
                });
        }

        [HttpPost("{id}/set-price")]
        public async Task<IActionResult> SetPrice(MovieId id, [FromBody] Money price)
        {
            var result = await _sender.Send(new SetMoviePriceCommand(id, price));

            return result.Match<IActionResult>(
                success: Ok, 
                error: BadRequest);
        }

        [HttpPost("{id}/publish")]
        public async Task<IActionResult> Publish(MovieId id)
        {
            var result = await _sender.Send(new PublishMovieCommand(id));

            return result.Match<IActionResult>(success: Ok, exception => exception switch
            {
                NotFoundException => NotFound(),
                PriceCheckPolicyFailure => StatusCode(StatusCodes.Status412PreconditionFailed, new { info = "Missing Price" }),
                _ => StatusCode(StatusCodes.Status500InternalServerError)
            });
        }
    }
}