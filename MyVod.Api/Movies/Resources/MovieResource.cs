using MyVod.Domain.Movies;
using MyVod.Domain.Shared;

namespace MyVod.Api.Movies.Resources
{
    public class CreateMovieResource
    {
        public string Title { get; set; }
        public string Description { get; set; }
    }

    public class MovieResource : CreateMovieResource
    {
        public MovieId Id { get; set; }
        public Money Price { get; set; }
        
        public static MovieResource From(Movie movie)
            => new MovieResource
            {
                Id = movie.Id,
                Description = movie.Description,
                Title = movie.Title,
                Price = movie.Price 
            };
    }
}